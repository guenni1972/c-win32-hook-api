﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 01-01-2015
 * 
 */

using System;
using System.Threading;
using System.Windows.Forms;
using CSharpWin32HookAPI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharpWin32HookAPITests
{
    [TestClass]
    public class Win32HookTests
    {

        [TestMethod]
        public void ForegroundIdleHookTest() // TODO: How to test this?
        {
            var doneEvent = new ManualResetEvent(false);

            Win32Hooks.ForegroundIdleHookEvent += (sender, e) =>
            {
                Console.WriteLine("wParam: " + e.WParam + " lParam: " + e.LParam);
                doneEvent.Set();
            };


            var eventReceived = doneEvent.WaitOne(1000);

            Assert.Inconclusive("How to test this?");
        }

        [TestMethod]
        public void GetMsgHookTest() // TODO: How to test this?
        {
            var doneEvent = new ManualResetEvent(false);

            Win32Hooks.GetMsgHookEvent += (sender, e) =>
            {
                doneEvent.Set();
            };

            var eventReceived = doneEvent.WaitOne(1000);

            Assert.Inconclusive("How to test this?");
        }
        
        [TestMethod]
        public void KeyboardHookTest() // TODO: How to test this?
        {
            var doneEvent = new ManualResetEvent(false);

            Win32Hooks.KeyboardHookEvent += (sender, e) =>
            {
                doneEvent.Set();
            };

            var eventReceived = doneEvent.WaitOne(1000);

            Assert.Inconclusive("How to test this?");
        }

        [TestMethod]
        public void LowLevelKeyboardHookTest()
        {
            var doneEvent = new ManualResetEvent(false);

            Win32Hooks.LowLevelKeyboardHookEvent += (sender, e) =>
            {
                var keyboardStruct = e.LParam;
                var key = keyboardStruct.vkCode;

                if (key == Keys.E && keyboardStruct.flags.IsAltDown)
                {
                    Console.WriteLine(key);
                    doneEvent.Set();
                }
            };

            // Send Alt + e
            SendKeys.SendWait("%e");

            var eventReceived = doneEvent.WaitOne(1000);

            Assert.IsTrue(eventReceived);
        }
    }
}

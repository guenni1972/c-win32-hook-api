﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 01-01-2015
 * 
 */

namespace CSharpWin32HookAPI
{
    /// <summary>
    /// Masks for the flags field in a KBDLLHOOKSTRUCT. See: http://msdn.microsoft.com/en-us/library/windows/desktop/ms644967(v=vs.85).aspx
    /// </summary>
    public enum LowLevelKeyboardHookFlagMasks : uint
    {
        /// <summary>
        /// Specifies whether the key is an extended key, such as a function key or a key on the numeric keypad. 
        /// The value is 1 if the key is an extended key; otherwise, it is 0.
        /// </summary>
        LLKHF_EXTENDED = 0x0000001,


        /// <summary>
        /// Specifies whether the event was injected from a process running at lower integrity level. 
        /// The value is 1 if that is the case; otherwise, it is 0. Note that bit 4 is also set whenever bit 1 is set.
        /// </summary>
        LLKHF_LOWER_IL_INJECTED = 0x00000002,

        /// <summary>
        /// Specifies whether the event was injected. 
        /// The value is 1 if that is the case; otherwise, it is 0. Note that bit 1 is not necessarily set when bit 4 is set.
        /// </summary>
        LLKHF_INJECTED = 0x00000010,

        /// <summary>
        /// The context code. The value is 1 if the ALT key is pressed; otherwise, it is 0.
        /// </summary>
        LLKHF_ALTDOWN = 0x00000020,

        /// <summary>
        /// The transition state. The value is 0 if the key is pressed and 1 if it is being released.
        /// </summary>
        LLKHF_UP = 0x00000080,

        /// <summary>
        /// Reserved.
        /// </summary>
        RESERVED = 0x0000004C,       
    }

    /// <summary>
    /// Masks for the flags field in a MSLLHOKSSTRUCT. See: http://msdn.microsoft.com/en-us/library/windows/desktop/ms644970(v=vs.85).aspx
    /// </summary>
    public enum LowLEvelMouseHookFlagMasks : uint
    {
        /// <summary>
        /// Test the event-injected (from any process) flag.
        /// </summary>
        LLMHF_INJECTED = 0x00000001,

        /// <summary>
        /// Test the event-injected (from a process running at lower integrity level) flag.
        /// </summary>
        LLMHF_LOWER_IL_INJECTED = 0x00000002
    }

    /// <summary>
    /// Masks for the Flags in a Keystroke Message. See: http://msdn.microsoft.com/en-us/library/windows/desktop/ms646267(v=vs.85).aspx#_win32_Keystroke_Message_Flags
    /// </summary>
    public enum KeystrokeMessageFlagMasks : uint
    {        
        /// <summary>
        /// You can check the repeat count to determine whether a keystroke message represents more than one keystroke. 
        /// The system increments the count when the keyboard generates WM_KEYDOWN or WM_SYSKEYDOWN messages faster than an application can process them. 
        /// This often occurs when the user holds down a key long enough to start the keyboard's automatic repeat feature. 
        /// Instead of filling the system message queue with the resulting key-down messages, 
        /// the system combines the messages into a single key down message and increments the repeat count. 
        /// Releasing a key cannot start the automatic repeat feature, so the repeat count for WM_KEYUP and WM_SYSKEYUP messages is always set to 1.
        /// </summary>
        REPEAT_COUNT = 0x0000FFFF,

        /// <summary>
        /// The scan code is the value that the keyboard hardware generates when the user presses a key. 
        /// It is a device-dependent value that identifies the key pressed, as opposed to the character represented by the key. 
        /// An application typically ignores scan codes. 
        /// Instead, it uses the device-independent virtual-key codes to interpret keystroke messages.
        /// </summary>
        SCAN_CODE = 0x00ff0000,

        /// <summary>
        /// The extended-key flag indicates whether the keystroke message originated from one of the additional keys on the enhanced keyboard. 
        /// The extended keys consist of the ALT and CTRL keys on the right-hand side of the keyboard; 
        /// the INS, DEL, HOME, END, PAGE UP, PAGE DOWN, and arrow keys in the clusters to the left of the numeric keypad; 
        /// the NUM LOCK key; the BREAK (CTRL+PAUSE) key; the PRINT SCRN key; and the divide (/) and ENTER keys in the numeric keypad. 
        /// The extended-key flag is set if the key is an extended key.
        /// </summary>
        EXTENDED_KEY = 0x01000000,

        /// <summary>
        /// Reserved.
        /// </summary>
        RESERVED = 0x1E000000,

        /// <summary>
        /// The context code indicates whether the ALT key was down when the keystroke message was generated. 
        /// The code is 1 if the ALT key was down and 0 if it was up.
        /// </summary>
        CONTEXT_CODE = 0x20000000,

        /// <summary>
        /// The previous key-state flag indicates whether the key that generated the keystroke message was previously up or down. 
        /// It is 1 if the key was previously down and 0 if the key was previously up. 
        /// You can use this flag to identify keystroke messages generated by the keyboard's automatic repeat feature. 
        /// This flag is set to 1 for WM_KEYDOWN and WM_SYSKEYDOWN keystroke messages generated by the automatic repeat feature.
        /// It is always set to 0 for WM_KEYUP and WM_SYSKEYUP messages.
        /// </summary>
        PREVIOUS_STATE = 0x40000000,

        /// <summary>
        /// The transition-state flag indicates whether pressing a key or releasing a key generated the keystroke message. 
        /// This flag is always set to 0 for WM_KEYDOWN and WM_SYSKEYDOWN messages; it is always set to 1 for WM_KEYUP and WM_SYSKEYUP messages.
        /// </summary>
        TRANSITION_STATE = 0x80000000,
    }
}

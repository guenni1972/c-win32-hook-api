﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 02-01-2015
 * 
 */

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CSharpWin32HookAPI.Extensions
{
    public enum VirtualKeyMapTypes
    {
        /// <summary>
        /// uCode is a virtual-key code and is translated into a scan code. 
        /// If it is a virtual-key code that does not distinguish between left- and right-hand keys, the left-hand scan code is returned. 
        /// If there is no translation, the function returns 0.
        /// </summary>
        MAPVK_VK_TO_VSC = 0,

        /// <summary>
        /// uCode is a scan code and is translated into a virtual-key code that does not distinguish between left- and right-hand keys. 
        /// If there is no translation, the function returns 0.
        /// </summary>
        MAPVK_VSC_TO_VK = 1,

        /// <summary>
        /// uCode is a virtual-key code and is translated into an unshifted character value in the low-order word of the return value. 
        /// Dead keys (diacritics) are indicated by setting the top bit of the return value.
        ///  If there is no translation, the function returns 0.
        /// </summary>
        MAPVK_VK_TO_CHAR = 2,

        /// <summary>
        /// uCode is a scan code and is translated into a virtual-key code that distinguishes between left- and right-hand keys.
        /// If there is no translation, the function returns 0.
        /// </summary>
        MAPVK_VSC_TO_VK_EX = 3,
    }

    public static class KeysExtensions
    {

        /// <summary>
        /// Translates (maps) a virtual-key code into a scan code or character value, or translates a scan code into a virtual-key code.
        /// </summary>
        /// <param name="uCode">The virtual key code or scan code for a key. How this value is interpreted depends on the value of the uMapType parameter.</param>
        /// <param name="uMapType">The translation to be performed. The value of this parameter depends on the value of the uCode parameter.</param>
        /// <returns>The return value is either a scan code, a virtual-key code, or a character value, depending on the value of uCode and uMapType. If there is no translation, the return value is zero.</returns>
        [DllImport("user32.dll")]
        static extern int MapVirtualKey(uint uCode, uint uMapType);

        public static char ToChar(this Keys keyCode)
        {
            var nonVirtualKey = MapVirtualKey((uint)keyCode, (uint)VirtualKeyMapTypes.MAPVK_VK_TO_CHAR);

            return Convert.ToChar(nonVirtualKey);
        }
    }
}

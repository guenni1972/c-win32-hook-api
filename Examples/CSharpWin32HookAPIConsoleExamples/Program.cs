﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 02-01-2015
 * 
 */

using System;
using System.Windows.Forms;
using CSharpWin32HookAPI;

namespace CSharpWin32HookAPIConsoleExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "C# Win32 Hook API Console Sample";

            // Register the Low Level Keyboard and Mouse hooks.
            // A managed console program only supports the low level hooks since it has no Win32 window assigned to it
            // and since it is managed it can't register non low-level global hooks.
            Win32Hooks.LowLevelKeyboardHookEvent += Win32HooksOnLowLevelKeyboardHookEvent;
            Win32Hooks.LowLevelMouseHookEvent += Win32HooksOnLowLevelMouseHookEvent;

            // Create a fake Win32 message loop for the low level hooks to run on.
            Application.Run();

            // Exit program when the window has focus and the escape key is pressed.
            while (Console.ReadKey().Key != ConsoleKey.Escape)
            {
            }

            // Unregister the hooks before exiting.
            Win32Hooks.LowLevelKeyboardHookEvent -= Win32HooksOnLowLevelKeyboardHookEvent;
            Win32Hooks.LowLevelMouseHookEvent -= Win32HooksOnLowLevelMouseHookEvent;
        }


        private static void Win32HooksOnLowLevelKeyboardHookEvent(object sender, Win32HookEventArgs<WM, KBDLLHOOKSTRUCT> win32HookEventArgs)
        {
            var infoString = "Keyboard Hook Fired." + "\n" +
                             " Type: " + win32HookEventArgs.WParam + "\n" +
                             " vkCode: " + win32HookEventArgs.LParam.vkCode + "\n" +
                             " scanCode: " + win32HookEventArgs.LParam.scanCode + "\n" +
                             " time: " + win32HookEventArgs.LParam.time + "\n" +
                             " dwExtraInfo: " + win32HookEventArgs.LParam.dwExtraInfo + "\n" +
                             " flags: " + "\n" + win32HookEventArgs.LParam.flags;

            Console.WriteLine(infoString);
        }
        private static void Win32HooksOnLowLevelMouseHookEvent(object sender, Win32HookEventArgs<WM, MSLLHOOKSTRUCT > win32HookEventArgs)
        {
            var infoString = "Low Level Mouse Hook Fired." + "\n" +
                             " Type: " + win32HookEventArgs.WParam + "\n" +
                             " pt: " + win32HookEventArgs.LParam.pt.x + "," + win32HookEventArgs.LParam.pt.y + "\n";

            // TODO: Implement something better then having to shift mouseData manually (how to do this type-safe?)
            if (win32HookEventArgs.WParam == WM.MOUSEWHEEL ||
                win32HookEventArgs.WParam == WM.MOUSEHWHEEL)
            {
                infoString += " Wheel Delta: " + (win32HookEventArgs.LParam.mouseData >> 16) + "\n";
            }
            else if (win32HookEventArgs.WParam == WM.XBUTTONDOWN ||
                     win32HookEventArgs.WParam == WM.XBUTTONUP ||
                     win32HookEventArgs.WParam == WM.XBUTTONDBLCLK ||
                     win32HookEventArgs.WParam == WM.NCXBUTTONDOWN ||
                     win32HookEventArgs.WParam == WM.NCXBUTTONUP ||
                     win32HookEventArgs.WParam == WM.NCXBUTTONDBLCLK)
            {
                infoString += " X Button: " + (win32HookEventArgs.LParam.mouseData >> 16) + "\n";
            }

            infoString += " time: " + win32HookEventArgs.LParam.time + "\n" +
                          " dwExtraInfo: " + win32HookEventArgs.LParam.dwExtraInfo + "\n" +
                          " flags: " + "\n" + win32HookEventArgs.LParam.flags;

            Console.WriteLine(infoString);
        }
    }
}

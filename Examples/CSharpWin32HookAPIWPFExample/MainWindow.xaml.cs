﻿/*
 * This file is part of the C# Win32 Hook API.
 *  
 * The C# Win32 Hook API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The C# Win32 Hook API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the C# Win32 Hook API. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Filename: Win32Hooks.cs
 * 
 * Authors: 
 * -Maarten Thomassen (m.j.l.h.thomassen@gmail.com)
 * 
 * Last Modified: 03-01-2015
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Timers;
using CSharpWin32HookAPI;
using CSharpWin32HookAPI.Extensions;
using Timer = System.Timers.Timer;

namespace CSharpWin32HookAPIWPFExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private Timer _logTimer;
        private bool _loggingEnabled = true;
        private readonly List<string> _log = new List<string>();

        public MainWindow()
        {
            InitializeComponent();

            // Register hook events by checking the checkboxes.
            CBTHookCheckBox.IsChecked = false;
            ForegroundIdleHookCheckBox.IsChecked = false;
            JournalRecordHookCheckBox.IsChecked = false;
            KeyboardHookCheckBox.IsChecked = false;
            MouseHookCheckBox.IsChecked = false;
            LowLevelKeyboardHookCheckBox.IsChecked = false;
            LowLevelMouseHookCheckBox.IsChecked = false;
            ShellHookCheckBox.IsChecked = true;

            _logTimer = new Timer(1000);
            _logTimer.Elapsed += LogTimerOnElapsed;
            _logTimer.Start();

        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            // Unregister hook events by unchecking the checkboxes.
            CBTHookCheckBox.IsChecked = false;
            ForegroundIdleHookCheckBox.IsChecked = false;
            JournalRecordHookCheckBox.IsChecked = false;
            KeyboardHookCheckBox.IsChecked = false;
            MouseHookCheckBox.IsChecked = false;
            LowLevelKeyboardHookCheckBox.IsChecked = false;
            LowLevelMouseHookCheckBox.IsChecked = false;
            ShellHookCheckBox.IsChecked = false;
        }

        private void LogTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {

            string logText;
            lock (_log)
            {
                if (!_log.Any() || !_loggingEnabled)
                {
                    return;
                }

                logText = _log.Aggregate("", (current, entry) => current + entry);
                _log.Clear();
            }

            Dispatcher.InvokeAsync(() =>
            {
                LogTextBox.Text += logText;
                LogTextBox.ScrollToEnd();
            });
        }

        private void EnableLoggingCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            _loggingEnabled = true;
        }

        private void EnableLoggingCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            _loggingEnabled = false;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            LogTextBox.Text = string.Empty;
        }


        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog();
            var dialogResult = dialog.ShowDialog();

            if (dialogResult == System.Windows.Forms.DialogResult.OK)
            {
                using (var file = dialog.OpenFile())
                {
                    using (var writer = new StreamWriter(file))
                    {
                        writer.Write(LogTextBox.Text);
                    }
                }
            }
        }

        private void WriteToLog(string text)
        {
            lock (_log)
            {
                var logText = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " - " + text;
                _log.Add(logText);
                Dispatcher.InvokeAsync(() =>
                {
                    LastEventTextBlock.Text = logText;
                });
            }
        }

        #region Hook Event Callbacks

        private void Win32HooksOnCbtHookEvent(object sender, Win32HookEventArgs<dynamic, dynamic> win32HookEventArgs)
        {
            var infoString = "CBT Hook Fired." + "\n" +
                             " Hook Type: " + (HCBT) win32HookEventArgs.NCode + "\n";

            if (win32HookEventArgs.IsHcbtSysCommandEventArgs())
            {
                var e = win32HookEventArgs.AsHcbtSysCommandEventArgs();
                infoString += " System Command: " + e.WParam + "\n";

                if (e.WParam == SC.KEYMENU)
                {
                    infoString += " Key: " + e.LParam.CharacterCode + "\n";
                }
                else
                {
                    infoString +=  " Cursor Position: " + e.LParam.X + "," + e.LParam.Y + "\n";
                }
            }
            else if(win32HookEventArgs.IsHcbtCreateWndEventArgs())
            {
                var e = win32HookEventArgs.AsHcbtCreateWndEventArgs();
                infoString += " Window Handle " + e.WParam + "\n" +
                              " hwndInsertAfter: " + e.LParam.hwndInsertAfter + "\n";

                var createStruct = e.LParam.lpcs.MarshalCbtCreateWndLpcs();
                infoString += " CreateStruct hwndParent: " + createStruct.hwndParent + "\n" +
                              " CreateStruct x,y: " + createStruct.x + "," + createStruct.y + "\n" +
                              " CreateStruct cx,cy: " + createStruct.cx + "," + createStruct.cy + "\n" +
                              " CreateStruct style: " + createStruct.style + "\n";

            }

            switch ((HCBT) win32HookEventArgs.NCode)
            {

                case  HCBT.ACTIVATE:
                    var activateEventrgs = win32HookEventArgs.AsHcbtActivateEventArgs();
                    infoString += " fMouse: " + activateEventrgs.LParam.fMouse + "\n" +
                                  " hWndActive: " + activateEventrgs.LParam.hWndActive + "\n";
                    break;
                case HCBT.CLICKSKIPPED:
                    var clickSkippedEventArgs = win32HookEventArgs.AsHcbtClickSkippedEventArgs();
                    infoString += " pt: " + clickSkippedEventArgs.LParam.pt.x + "," + clickSkippedEventArgs.LParam.pt.y + "\n" +
                                  " hwnd: " + clickSkippedEventArgs.LParam.hwnd + "\n" +
                                  " wHitTestCode: " + clickSkippedEventArgs.LParam.wHitTestCode + "\n" +
                                  " dwExtraInfo: " + clickSkippedEventArgs.LParam.dwExtraInfo + "\n";
                    break;
                default:
                    break;
            }

            WriteToLog(infoString);
        }

        private void Win32HooksOnDebugHookEvent(object sender, Win32HookEventArgs<WH, DEBUGHOOKINFO> win32HookEventArgs)
        {
            var lParam = (MSG) win32HookEventArgs.LParam.lParam.MarshalDebugHookLParam(win32HookEventArgs.WParam);

            if (!Enum.IsDefined(typeof(WM), lParam.message))
            {
                return;
            }

            var infoString = "Debug Hook Fired." + "\n" +
                             " Hook Type: " + win32HookEventArgs.WParam + "\n" +
                             " idThread:  0x" + win32HookEventArgs.LParam.idThreadInstaller.ToString("X8") + "\n" +
                             " idThreadInstaller: 0x" + win32HookEventArgs.LParam.idThreadInstaller.ToString("X8") + "\n" +
                             " lParam:  0x" + win32HookEventArgs.LParam.lParam.ToString("X8") + "\n" +
                             " wParam:  0x" + win32HookEventArgs.LParam.wParam.ToString("X8") + "\n" +
                             " code: " + win32HookEventArgs.LParam.code + "\n";

            WriteToLog(infoString);
        }

        private void Win32HooksOnForegroundIdleHookEvent(object sender, Win32HookEventArgs<UnusedParam, UnusedParam> win32HookEventArgs)
        {
            var infoString = "Foreground Idle Hook Fired." + "\n";

            WriteToLog(infoString);
        }

        private void Win32HooksOnGetMsgHookEvent(object sender, Win32HookEventArgs<PM, MSG> win32HookEventArgs)
        {
            if (!Enum.IsDefined(typeof(WM), win32HookEventArgs.LParam.message))
            {
                return;
            }

            var infoString = "Get Message Hook Fired." + "\n" +
                             " Message Queue Action: " + win32HookEventArgs.WParam + "\n" +
                             " hwnd:  0x" + win32HookEventArgs.LParam.hwnd.ToString("X8") + "\n" +
                             " message: " + win32HookEventArgs.LParam.message + "\n" +
                             " wParam:  0x" + win32HookEventArgs.LParam.wParam.ToString("X8") + "\n" +
                             " lParam:  0x" + win32HookEventArgs.LParam.lParam.ToString("X8") + "\n" +
                             " time: " + win32HookEventArgs.LParam.time + "\n" +
                             " pt: " + win32HookEventArgs.LParam.pt.x + "," + win32HookEventArgs.LParam.pt.y + "\n";

            WriteToLog(infoString);
        }


        private void Win32HooksOnJournalRecordHookEvent(object sender, Win32HookEventArgs<UnusedParam, EVENTMSG> win32HookEventArgs)
        {
            var infoString = "Journal Record Hook Fired." + "\n" +
                             " message: " + win32HookEventArgs.LParam.message + "\n" +
                             " paramL: " + win32HookEventArgs.LParam.paramL + "\n" +
                             " paramH: " + win32HookEventArgs.LParam.paramH + "\n" +
                             " time: " + win32HookEventArgs.LParam.time + "\n" +
                             " hwnd: " + win32HookEventArgs.LParam.hwnd + "\n";

            WriteToLog(infoString);
        }

        private void Win32HooksOnKeyboardHookEvent(object sender, Win32HookEventArgs<Keys, KeystrokeMessageFlags> win32HookEventArgs)
        {
            var infoString = "Keyboard Hook Fired." + "\n" +
                             " Key: " + win32HookEventArgs.WParam + "\n" +
                             win32HookEventArgs.LParam;

            WriteToLog(infoString);
        }

        private void Win32HooksOnMouseHookEvent(object sender, Win32HookEventArgs<WM, MOUSEHOOKSTRUCT > win32HookEventArgs)
        {
            var infoString = "Mouse Hook Fired." + "\n" +
                             " Type: " + win32HookEventArgs.WParam + "\n" +
                             " pt: " + win32HookEventArgs.LParam.pt.x + "," + win32HookEventArgs.LParam.pt.y + "\n" +
                             " hwnd: " + win32HookEventArgs.LParam.hwnd + "\n" + 
                             " wHitTestCode: " + win32HookEventArgs.LParam.wHitTestCode + "\n" +
                             " dwExtraInfo: " + win32HookEventArgs.LParam.dwExtraInfo + "\n";
            
            WriteToLog(infoString);
        }

        private void Win32HooksOnLowLevelKeyboardHookEvent(object sender, Win32HookEventArgs<WM, KBDLLHOOKSTRUCT> win32HookEventArgs)
        {
            var infoString = "Keyboard Hook Fired." + "\n" +
                             " Type: " + win32HookEventArgs.WParam + "\n" +
                             " vkCode: " + win32HookEventArgs.LParam.vkCode + "\n" +
                             " scanCode: " + win32HookEventArgs.LParam.scanCode + "\n" +
                             " time: " + win32HookEventArgs.LParam.time + "\n" +
                             " dwExtraInfo: " + win32HookEventArgs.LParam.dwExtraInfo + "\n" +
                             " flags: " + "\n" + win32HookEventArgs.LParam.flags;

            WriteToLog(infoString);
        }
        private void Win32HooksOnLowLevelMouseHookEvent(object sender, Win32HookEventArgs<WM, MSLLHOOKSTRUCT > win32HookEventArgs)
        {
            var infoString = "Low Level Mouse Hook Fired." + "\n" +
                             " Type: " + win32HookEventArgs.WParam + "\n" +
                             " pt: " + win32HookEventArgs.LParam.pt.x + "," + win32HookEventArgs.LParam.pt.y + "\n";

            // TODO: Implement something better then having to shift mouseData manually (how to do this type-safe?)
            if (win32HookEventArgs.WParam == WM.MOUSEWHEEL ||
                win32HookEventArgs.WParam == WM.MOUSEHWHEEL)
            {
                infoString += " Wheel Delta: " + (win32HookEventArgs.LParam.mouseData >> 16) +  "\n";
            }
            else if (win32HookEventArgs.WParam == WM.XBUTTONDOWN ||
                     win32HookEventArgs.WParam == WM.XBUTTONUP ||
                     win32HookEventArgs.WParam == WM.XBUTTONDBLCLK ||
                     win32HookEventArgs.WParam == WM.NCXBUTTONDOWN ||
                     win32HookEventArgs.WParam == WM.NCXBUTTONUP ||
                     win32HookEventArgs.WParam == WM.NCXBUTTONDBLCLK)
            {
                infoString += " X Button: " + (win32HookEventArgs.LParam.mouseData >> 16) +  "\n";
            }

            infoString += " time: " + win32HookEventArgs.LParam.time + "\n" +
                          " dwExtraInfo: " + win32HookEventArgs.LParam.dwExtraInfo + "\n" +
                          " flags: " + "\n" + win32HookEventArgs.LParam.flags;

            WriteToLog(infoString);
        }

        private void Win32HooksOnShellHookEvent(object sender, Win32HookEventArgs<HSHELL, object, object> win32HookEventArgs)
        {
            var infoString = "Shell Hook Fired." + "\n" +
                             " Hook Type: " + win32HookEventArgs.NCode + "\n";

            if (win32HookEventArgs.NCode == HSHELL.GETMINRECT)
            {
                var e = win32HookEventArgs.AsHshellGetMinRectState();
                infoString += " Window Handle: " + e.WParam + "\n" +
                              " Rect: " + e.LParam.left + "," + e.LParam.top + "," + e.LParam.right + "," + e.LParam.bottom + "\n"; 
            }
            if (win32HookEventArgs.IsHshellWindowActivated())
            {
                var e = win32HookEventArgs.AsHshellWindowActivated();
                infoString += " Window Handle: " + e.WParam + "\n" +
                              " Is Full-Screen: " + e.LParam + "\n";
            }

            WriteToLog(infoString);
        }

        #endregion

        #region Checkbox Callbacks

        private void CBTHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            //Win32Hooks.add_CBTHookEvent(Win32HooksOnCbtHookEvent);
            Win32Hooks.CBTHookEvent += Win32HooksOnCbtHookEvent;
        }

        private void CBTHookCheckBox_OnUncheckedCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            //Win32Hooks.remove_CBTHookEvent(Win32HooksOnCbtHookEvent);
            Win32Hooks.CBTHookEvent -= Win32HooksOnCbtHookEvent;
        }

        private void DebugHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.DebugHookEvent += Win32HooksOnDebugHookEvent;
        }

        private void DebugHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.DebugHookEvent -= Win32HooksOnDebugHookEvent;
        }

        private void ForegroundIdleHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.ForegroundIdleHookEvent += Win32HooksOnForegroundIdleHookEvent;
        }

        private void ForegroundIdleHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.ForegroundIdleHookEvent -= Win32HooksOnForegroundIdleHookEvent;
        }

        private void GetMsgHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.GetMsgHookEvent += Win32HooksOnGetMsgHookEvent;
        }

        private void GetMsgHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.GetMsgHookEvent -= Win32HooksOnGetMsgHookEvent;
        }
        private void JournalRecordHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.JournalRecordHookEvent += Win32HooksOnJournalRecordHookEvent;
        }

        private void JournalRecordHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.JournalRecordHookEvent -= Win32HooksOnJournalRecordHookEvent;
        }

        private void KeyboardHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.KeyboardHookEvent += Win32HooksOnKeyboardHookEvent;
        }

        private void KeyboardHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.KeyboardHookEvent -= Win32HooksOnKeyboardHookEvent;
        }

        private void MouseHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.MouseHookEvent += Win32HooksOnMouseHookEvent;
        }

        private void MouseHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.MouseHookEvent -= Win32HooksOnMouseHookEvent;
        }

        private void LowLevelKeyboardHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.LowLevelKeyboardHookEvent += Win32HooksOnLowLevelKeyboardHookEvent;
        }

        private void LowLevelKeyboardHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.LowLevelKeyboardHookEvent -= Win32HooksOnLowLevelKeyboardHookEvent;
        }

        private void LowLevelMouseHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.LowLevelMouseHookEvent += Win32HooksOnLowLevelMouseHookEvent;
        }

        private void LowLevelMouseHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.LowLevelMouseHookEvent -= Win32HooksOnLowLevelMouseHookEvent;
        }

        private void ShellHookCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.ShellHookEvent += Win32HooksOnShellHookEvent;
        }

        private void ShellHookCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            Win32Hooks.ShellHookEvent -= Win32HooksOnShellHookEvent;
        }
        #endregion
    }
}
